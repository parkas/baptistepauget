# Spécification de la mémoire dans un langage synchrone flot de données doté de tableaux de taille statique

Ce dépot contient mon manuscrit de thèse et le support utilisé lors de la soutenance.


## Résumé


Scade est un langage de programmation synchrone utilisé depuis la fin des années 1990 pour concevoir et implémenter des systèmes critiques embarqués tels ceux que l'on trouve dans l'aviation.
Cette criticité requiert la formalisation (i) des programmes, afin de garantir qu'ils s'exécutent sans erreurs ou retards pendant une durée arbitraire et (ii) des outils de développement, en particulier le compilateur, pour s'assurer de la préservation des propriétés des modèles lors de la génération de code.
Ces activités reposent sur une description flot de données des modèles, inspirée des schémas-blocs, dont la sémantique s'exprime par des fonctions de suites, ainsi que d'une compilation précisément documentée.

Scade descend de Lustre. La version 6 a introduit des évolutions majeures, dans les pas de LucidSynchrone, dont les automates hiérarchiques et les tableaux. Pour ces derniers, leur taille est connue et vérifiée statiquement afin d'assurer un temps d'exécution et une mémoire bornés.
L'utilisation croissante des tableaux de Scade pour des opérations intensives a révélé plusieurs axes d'amélioration: verbosité des modèles, manque de contrôle de la compilation ou description peu commode de certains algorithmes.

Dans cette thèse, ces trois aspects ont été étudiés à l'aide d'un prototype de compilateur.
(i) Nous avons développé un système de types _a la_ Hindley-Milner spécifiant les tailles sous la forme de polynômes multivariés. Cette proposition permet de vérifier et d'inférer la plupart des tailles de manière modulaire.
(ii) Nous avons exploré une méthode de compilation alternative, fondée sur un _langage déclaratif conscient de la mémoire_. Il vise à concilier le style flot de données avec une spécification précise des emplacements mémoire. La description modulaire des tailles en est un élément clé.
(iii) Enfin, nous proposons une construction d'itération inspirée de Sisal qui complète les itérateurs actuels. En traitant les tableaux comme des suites finies, elle donne accès aux constructions séquentielles de Scade (automates) lors d'itérations. De plus, elle permet de décrire de manière déclarative des implémentations efficaces d'algorithmes comme la décomposition de Cholesky. Cette compilation contrôlable est un premier pas nécessaire pour la compilation vers des GPUs.
    
    
# Memory Specification in a Data-flow Synchronous Language with Statically Sized Arrays    

This repository contains my PhD manuscript and the slides used for the defense.

## Abstract


The synchronous programming language Scade has been used since the end of the 1990s to design safety critical embedded systems such as those found in avionics.
This context requires to formally reason about (i) the programs, to ensure that they run for an arbitrary long time without errors or missed deadlines
and (ii) the tools, in particular the compiler, to give high confidence in the preservation of model properties through the code generation process.
These activities build on a data-flow description of models, inspired by block diagrams, that enjoys a formal semantics in terms of streams and a well-documented compilation process.

Scade stems from Lustre. The Scade6 version introduced major evolutions following LucidSynchrone, notably hierarchical automata and arrays. For the latter, sizes are statically known and checked, so as to fulfill the bounded resources and execution time constraints.
The increasing use of Scade arrays for data-intensive computations has revealed areas for improvement: concision of the models, control of the generated code and idiomatic description of array computations.

Using a prototype implementation of a compiler, the present work investigates these three aspects. 
(i) We designed a Hindley-Milner-like type system for representing sizes with multivariate polynomials. This proposal allows to check and infer most sizes in a modular way.
(ii) We explored an alternative compilation process based on a _memory-aware declarative language_. It aims at reconciling the data-flow style with a precise specification of memory locations. The underlying memory model builds on our modular description of sizes.
(iii) Last, we propose an iteration construct inspired by Sisal that supplements the available iterators. 
By viewing arrays as finite streams, iteration can benefit from the sequential constructs of Scade, _e.g.,_ automata.
Moreover, it allows declarative descriptions of efficient algorithm implementations such as the Cholesky decomposition.
This controllable compilation process is a mandatory step toward code generation for GPUs.
